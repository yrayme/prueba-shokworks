import React from "react";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import about from "../public/img/what-can-you-build.png";

export default function About() {
  return (
    <div className={styles.container}>
      <div className={styles.row}>
        <div className={styles.col_6}>
          <div className={styles.aboutText}>
            <p className={styles.title}>The Process</p>
            <p className={styles.title} style={{ marginTop: -25 }}>
              About Our Work
            </p>
            <p>
              Alejandro Laplana leads and adaptable end-to-end development team
              consisting of a large portion of captable enterprise mixed reality
              solutions.
            </p>

            <button
              href="#"
              className={styles.button1}
              style={{ marginLeft: 0, marginTop: 20 }}
            >
              Know More
            </button>
          </div>
        </div>
        <div className={styles.col_6}>
          <Image src={about} alt="user" />
        </div>
      </div>
    </div>
  );
}
