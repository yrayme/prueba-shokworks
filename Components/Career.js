import React from "react";
import styles from "../styles/Home.module.css";
import CardCareer from "./CardCareer";
import image1 from "../public/img/big immersity logo.png";
import image2 from "../public/img/logo_ipass.png"
import image3 from "../public/img/logo-rojo.png"
import image4 from "../public/img/logo_splash.svg"
import image5 from "../public/img/Group 94.png"
import image6 from "../public/img/Group 2788.svg"

export default function Career() {
  return (
    <div className={styles.containerCareer} style={{ marginTop: 100 }}>
      <div className={styles.center} style={{ fontWeight: "bold" }}>
        <p className={styles.title}>Our Partnes & Clients</p>
      </div>
      <div className={styles.row}>
        <div className={styles.col_3} >
          <div className={styles.card}>
            <CardCareer image={image1}/>
          </div>
        </div>
        <div className={styles.col_3a}>
          <div className={styles.card}>
            <CardCareer  image={image2}/>
          </div>
        </div>
        <div className={styles.col_3} >
          <div className={styles.card}>
            <CardCareer  image={image3}/>
          </div>
        </div>
        <div className={styles.col_3} >
          <div className={styles.card}>
            <CardCareer  image={image4}/>
          </div>
        </div>
        <div className={styles.col_3a}>
          <div className={styles.card}>
            <CardCareer  image={image5}/>
          </div>
        </div>
        <div className={styles.col_3}>
          <div className={styles.card}>
            <CardCareer  image={image6}/>
          </div>
        </div>
      </div>
    </div>
  );
}
