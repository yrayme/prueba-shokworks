import React from "react";
import styles from "../styles/Home.module.css";
import Image from "next/image";

export default function CardCareer(props) {
  return (
    <div>
      <div className={styles.center}>
        {props.image && (
          <Image src={props.image} alt="user" className={styles.sizeImage} />
        )}
        <p>Learn more</p>
      </div>
    </div>
  );
}
