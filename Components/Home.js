import React from "react";
import Image from "next/image";
import home from "../public/img/community1.png";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.fondo}>
      <div className={styles.center}>
        <Image src={home} alt="user" />
        <h1>A Brand New Way </h1>
        <h1 style={{ marginTop: -20 }}>to See The Word</h1>
        {/* <div> */}

        <button href="#" className={styles.button}>
          Our Works
        </button>
        <button href="#" className={styles.button1}>
          Know More
        </button>
        {/* </div> */}
      </div>
    </div>
  );
}
