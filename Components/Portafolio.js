import React from "react";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import image from "../public/img/article-image-5.png";

export default function Portafolio() {
  return (
    <div className={styles.containerPortafolio}>
      <div className={styles.row}>
        <div className={styles.col_6}>
          <div className={styles.aboutText}>
            <Image src={image} alt="user" />
          </div>
        </div>
        <div className={styles.col_6}>
          <div className={styles.aboutText}>
            <p className={styles.title}>We arfe here to</p>
            <p className={styles.title} style={{ marginTop: -25 }}>
              <span style={{ fontWeight: "bold" }}>always help</span> you
            </p>
            <p>
              Shokworks team provide solutions and guidance to every project,
              taking the project vision to high level.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
