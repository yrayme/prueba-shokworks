import React from "react";
import styles from "../styles/Home.module.css";
import Image from "next/image";
import image from "../public/img/banner-1.png";

export default function Contact() {
  return (
    <div className={styles.fondoContact} style={{ marginTop: 50 }}>
      <div className={styles.containerContact}>
        <div className={styles.center}>
          <p className={styles.title}>Facing Problem?</p>
          <p
            className={styles.title}
            style={{ marginTop: -25, fontWeight: "bold" }}
          >
            Lets Get In Touch Now
          </p>
        </div>
        <div className={styles.row}>
          <div className={styles.col_7}>
            <form className={styles.form}>
              <div className={styles.row}>
                <div className={styles.col_6}>
                  <p type="First Name" className={styles.label}>
                    <input
                      placeholder="Robert lee"
                      className={styles.input}
                    ></input>
                  </p>
                </div>
                <div className={styles.col_6}>
                  <p type="Last Name" className={styles.label}>
                    <input
                      placeholder="Anderson"
                      className={styles.input}
                    ></input>
                  </p>
                </div>
                <div className={styles.col_12}>
                  <p type="Yout Email Address" className={styles.label}>
                    <input
                      placeholder="prueba@gmail.com"
                      className={styles.input}
                    ></input>
                  </p>
                </div>
                <div className={styles.col_12}>
                  <p
                    type="Which Related Problem You Are Facing?"
                    className={styles.label}
                  >
                    <select
                      placeholder="Select One"
                      className={styles.input}
                    ></select>
                  </p>
                </div>
                <div className={styles.col_12}>
                  <p type="Type Your Message" className={styles.label}>
                    <textarea
                      rows="3"
                      placeholder="Here goes your message"
                      className={styles.input}
                    ></textarea>
                  </p>
                </div>
              </div>

              <button
                href="#"
                className={styles.button1}
                style={{ marginLeft: 0, marginTop: 10 }}
              >
                Our Works
              </button>
            </form>
          </div>
          <div className={styles.col_4a}>
            <div className={styles.hiddenMobile}>
              <Image src={image} alt="user" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
