import React from "react";
import styles from "../styles/Home.module.css";
import Image from "next/image";
import logo from "../public/img/logo.svg";
import image from "../public/img/contact.svg";
import face from "../public/img/facebook.svg";
import insta from "../public/img/instagram.svg";
import skype from "../public/img/skype.svg";
import Linked from "../public/img/linkedin.svg";

export default function Footer() {
  return (
    <div className={styles.fondoContact}>
      <div className={styles.forma}>
        <div className={styles.containerFooter}>
          <div className={styles.center} style={{ marginTop: 20 }}>
            <Image src={logo} alt="user" />
          </div>
          <div className={styles.row} style={{ marginTop: 30 }}>
            <div className={styles.col_4b}>
              <p className={styles.contactUs} style={{fontWeight: "bold"}}>Contact Us</p>
              <p>
                <span style={{ width: 50 }}>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>{" "}
                +(1) 824-5428
              </p>
              <p>
                <span style={{ width: 50 }}>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                Miami, United States
              </p>
              <p>
                <span style={{ width: 50 }}>
                  <Image src={image} alt="user" /> &nbsp;&nbsp;
                </span>
                yfuentes@shokWorks.io
              </p>
            </div>
            <div className={styles.col_4b}>
              <p style={{ fontWeight: "bold" }}>Our Company</p>
              <p>About</p>
              <p>Product</p>
              <p>Portafolio</p>
              <p>Career</p>
              <p>Blog</p>
            </div>
            <div className={styles.col_4b}>
              <p style={{ fontWeight: "bold" }}>Social Contacts</p>
              <p>Facebook</p>
              <p>Linked In</p>
              <p>Youtube</p>
              <p>Vimeo</p>
              <p>skype</p>
            </div>
            <div className={styles.col_4b}>
              <p style={{ fontWeight: "bold" }}>Address</p>
              <p>
                Internet Systems Consortium, Inc. 950 Charter Street Redwood
                City, CA USA.
              </p>
              <p style={{ fontWeight: "bold" }}>Follow Us</p>
              <div className={styles.row}>
                <div className={styles.col_4b}>
                  <div className={styles.facebook}>
                    <Image src={face} alt="user" />
                  </div>
                </div>
                <div className={styles.col_4b}>
                  <div className={styles.skype}>
                    <Image src={skype} alt="user" />
                  </div>
                </div>
                <div className={styles.col_4b}>
                  <div className={styles.linked}>
                    <Image src={Linked} alt="user" />
                  </div>
                </div>
                <div className={styles.col_4b}>
                  <div className={styles.instagram}>
                    <Image src={insta} alt="user" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
