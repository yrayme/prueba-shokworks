import React, { useEffect, useState } from "react";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import right from "../public/img/arrow-right.svg";
import left from "../public/img/arrow-left.svg";

function Product() {
  const [data, setdata] = useState([]);
  const [activeStep, setactiveStep] = useState(0);
  const articles = async () => {
    const res = await fetch(
      "https://newsapi.org/v2/everything?q=tesla&from=2021-09-13&sortBy=publishedAt&apiKey=b2189ffaab84419e9c1efbe881930c46"
    );
    const json = await res.json();
    var t2 = [];
    var articles = json.articles;
    if (window.innerWidth > 959) {
      while (articles.length > 0) {
        t2.push(articles.splice(0, 4));
      }
    } else if (window.innerWidth < 959 && window.innerWidth > 425) {
      while (articles.length > 0) {
        t2.push(articles.splice(0, 2));
      }
    } else if (window.innerWidth <= 425) {
      while (articles.length > 0) {
        t2.push(articles.splice(0, 1));
      }
    }
    setdata(t2);
  };
  useEffect(() => {
    articles();
  }, []);

  const handleBack = () => {
    if (activeStep === 0) {
        setactiveStep(data.length-1);
    } else {
        setactiveStep(activeStep - 1);
    }
  };

  const handleNext = () => {
    var length = data.length;
    console.log(length);
    if (activeStep === length-1) {
        setactiveStep(0);
    } else {
      setactiveStep(activeStep + 1);
    }
  };
  return (
    <div>
      <div className={styles.center}>
        <p className={styles.title}>What is the</p>
        <p
          className={styles.title}
          style={{ marginTop: -25, fontWeight: "bold" }}
        >
          Speciality of us
        </p>
      </div>
      <div className={styles.row}>
        {data.length > 0 &&
          data[activeStep].map((dat, id) => (
            <div className={styles.col_4} key={id} onClick={handleNext}>
              <div className={styles.center}>
                <img
                  src={dat.urlToImage}
                  alt="user"
                  height="120px"
                  width="120px"
                  style={{ borderRadius: 100 }}
                />
                <h3>{dat.author}</h3>
                <p>{dat.title}</p>
              </div>
            </div>
          ))}
      </div>
      <div className={styles.center}>
        <div className={styles.left} onClick={handleBack}>
          <Image src={left} alt="user" />
        </div>
        <div className={styles.right} onClick={handleNext}>
          <Image src={right} alt="user" />
        </div>
      </div>
    </div>
  );
}
export default Product;
