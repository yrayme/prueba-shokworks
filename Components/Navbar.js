import React, { useState } from "react";
import styles from "../styles/Home.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import Image from "next/image";
import logo from "../public/img/logo.svg";
import search from "../public/img/search.svg";

export default function Header() {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <div className={styles.header}>
      <nav className={styles.hiddenMobile}>
        <ul>
          <li className={styles.logo}>
            <a href="#">
              <Image src={logo} alt="user" width="200px" />
            </a>
          </li>
          <li>
            <a href="#">Home</a>
          </li>
          <li>
            <a href="#">About</a>
          </li>
          <li>
            <a href="#">Portafolio</a>
          </li>
          <li>
            <a href="#">Product</a>
          </li>
          <li>
            <a href="#">Career</a>
          </li>
          <li>
            <a href="#">Blog</a>
          </li>
          <li style={{ textAlign: "right" }}>
            <div className={styles.invisibleSearch}>
              <input placeholder="Search Here" className={styles.search} />
            </div>
            <div className={styles.image}>
              <Image src={search} alt="user" />
            </div>
          </li>
          <li style={{ textAlign: "right" }}>
            <button href="#" className={styles.button}>
              Contact Us
            </button>
          </li>
        </ul>
      </nav>
      <div className={styles.hiddenDesktop} style={{ padding: 20 }}>
        <Image src={logo} alt="user" width="200px" onClick={handleClick} />
      </div>
      {open && (
        <ul className={styles.menuMobile}>
          <li>
            <a href="#">Home</a>
          </li>
          <li>
            <a href="#">About</a>
          </li>
          <li>
            <a href="#">Portafolio</a>
          </li>
          <li>
            <a href="#">Product</a>
          </li>
          <li>
            <a href="#">Career</a>X
          </li>
          <li>
            <a href="#">Blog</a>
          </li>
          <li>
            <input placeholder="Search Here" className={styles.search} />
          </li>
          <li>
            <button href="#" className={styles.button}>
              Contact Us
            </button>
          </li>
        </ul>
      )}
    </div>
  );
}
